// When rolling two six-sided dice, each die will show a number between 1 and 6. When the numbers on the two dice are added together, they will sum to a number between 2 and 12.

// Use JavaScript to simulate 1000 rolls of a pair of dice. You will need to figure out how to generate random numbers in JavaScript, use your Google-Fu!

// For each possible roll (2 through 12), count its frequency. You should use an array to keep track of these counts.

// Initialize an array named count by filling it with zeros. Then, whenever the roll of the two dice adds up to, say, a seven, add one to the value in the 7th element of the counts array.

// For example if the variable rollOfDice holds the current roll, you could increment the count with code like:

// count[rollOfDice] = count[rollOfDice] + 1;
// After the 1000 rolls are finished, show the final counts in an HTML page.

// n addition to the numeric output described above, display the final counts as a bar graph (using a DIV for each bar, and varying the dimensions in proportion with the counts).
var theRoll;
// let myArray=[];
var myArray = Array(1000).fill(0)
console.log(myArray);
let ArrayCount = Array(13).fill(0)
var destination = document.getElementById("dice");


    theRoll = prompt("The Roll", 1000);

    for(let i=0; i < theRoll; i++)
    {
    const DiceRoll1 = Math.ceil(Math.random()*6);
    
    const DiceRoll2 = Math.ceil(Math.random()*6);
    
    myArray[i]=DiceRoll1+DiceRoll2;

    const temp = DiceRoll1+DiceRoll2;

    ArrayCount[temp]++;


}


for(let i=2; i<=12; i++){
    var newElement = document.createElement("div");
    newElement.style.height =  "20px";
    newElement.style.width = (ArrayCount[i]+200)+ "px";
    newElement.style.backgroundColor = "gray";
    newElement.style.margin = "10px";
    var newText = document.createTextNode(i+ " Was Rolled " +ArrayCount[i]);
    newElement.appendChild(newText);
    destination.appendChild(newElement);
}




theRoll = prompt("Rolling");